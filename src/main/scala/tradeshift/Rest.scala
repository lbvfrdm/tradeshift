package tradeshift

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server._
import akka.stream.ActorMaterializer
import spray.json.{DefaultJsonProtocol, DeserializationException, JsString, JsValue, RootJsonFormat}
import scala.io.StdIn


object Rest extends Directives with DefaultJsonProtocol with SprayJsonSupport {

  //web
  implicit val system = ActorSystem("tradeshift-rest")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  class Rest(service: Service) {

    val route =
      (path("children") & get & parameters('nodeId.as[String])) { (nodeId) =>

        val res = service.printChildren(nodeId)
        res match  {
          case Right(children) => complete(children)
          case Left(msg) => complete(msg)
        }

      } ~ (path("move") & post & parameters('nodeId.as[String], 'newParentId.as[String])) { (nodeId, newParentId) =>

        val res = service.moveTo(nodeId, newParentId)
        res match  {
          case Right(_) => complete("node is moved")
          case Left(msg) => complete(msg)
        }
      }
  }

  val host = "0.0.0.0"
  val port = 8080

  def main(args: Array[String]): Unit = {

    val perLayer = sys.env.get("tl").map(_.toInt).getOrElse(2)
    val deep = sys.env.get("td").map(_.toInt).getOrElse(2)
    val dataFolder = sys.env.get("datafolder").getOrElse("/tmp/")

    println(s"perLayer: $perLayer")
    println(s"deep: $deep")
    println(s"dataFolder: $dataFolder")

    val rest = new Rest(new Service(new Persistence(new Codec(), dataFolder), new Config(perLayer, deep)))
    val bindingFuture = Http().bindAndHandle(rest.route, host, port)

    println(s"Server online\nPress RETURN to stop...")
    StdIn.readLine()
    bindingFuture
      .flatMap(_.unbind())
      .onComplete(_ => system.terminate())
  }
}
