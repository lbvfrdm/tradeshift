package tradeshift

import java.io.File
import java.nio.file.{Files, Paths, StandardOpenOption}
import scala.io.Source


class Persistence(codec: Codec, dataFolder: String) {

  private val separator = ","
  private val dataFile = Paths.get(dataFolder + "data.txt")
  private val logFile = Paths.get(dataFolder + "log.txt")

  def check() = {

    if(!dataFile.toFile.exists() || !logFile.toFile.exists()) {

      def create(file: File): Unit = {
        if(!file.exists()){
          file.createNewFile()
        }
      }

      create(dataFile.toFile)
      create(logFile.toFile)
      Files.write(dataFile, Array.empty[Byte], StandardOpenOption.TRUNCATE_EXISTING);
      Files.write(logFile, Array.empty[Byte], StandardOpenOption.TRUNCATE_EXISTING);
      true

    } else {

      false
    }
  }

  def save(node: Node): Unit = {

    val serialized = codec.serialize(node);
    Files.write(dataFile, serialized.getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
    Files.write(logFile, Array.empty[Byte], StandardOpenOption.TRUNCATE_EXISTING);
  }

  def load(): Node = {

    val rootNode = codec.deserialize(Source.fromFile(dataFile.toFile).mkString)

    for(line <- Source.fromFile(logFile.toFile).getLines()) {

      val nodeId :: parentId :: Nil = line.split(separator).toList

      for {
        parent <- rootNode.findById(parentId)
        node <- rootNode.findById(nodeId)
      } yield parent.addAsChildren(node)

    }

    rootNode
  }


  def addNodeMoveLog(nodeId: String, newParentId: String) = {
    val diff = s"$nodeId$separator$newParentId${System.lineSeparator()}"
    Files.write(logFile, diff.getBytes(), StandardOpenOption.APPEND);
  }

}