package tradeshift

import scala.annotation.tailrec
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

class Node(val id: String) {

  var parent: Option[Node] = None
  var children = ArrayBuffer.empty[Node]

  def findById(id: String): Option[Node] = {

    @tailrec
    def rec(nodes: Seq[Node] = Seq(this)): Option[Node] = {

      if (nodes.isEmpty) {
        None
      } else {
        val optNode = nodes.find(_.id == id)
        optNode match {
          case Some(_) => optNode
          case None => rec(nodes.flatMap(_.children))
        }
      }
    }

    rec()
  }


  def addNewChildren(id: String): Node = {
    val node = new Node(id)
    node.parent = Some(this)
    children += node
    node
  }


  def addAsChildren(node: Node): Unit = {
    require(node.parent != None, "it is root node")

    val oldParent = node.parent.get
    oldParent.children -= node
    node.parent = Some(this)
    children += node
  }

  def allChildren: Seq[Node] = {

    @tailrec
    def rec(children: Seq[Node] = this.children, acc: ArrayBuffer[Node] = ArrayBuffer.empty): Seq[Node] = {

      if(children.isEmpty) {
        acc
      } else {
        rec(children.flatMap(_.children), acc ++= children)
      }
    }
    rec()
  }

  def height: Int = {

    @tailrec
    def rec(node: Node = this, acc: Int = 0): Int = {
      node.parent match  {
        case None => acc
        case Some(p) => rec(p, acc + 1)
      }
    }

    rec()
  }

  @tailrec
  final def root: Node = {
    parent match {
      case None => this
      case Some(p) => p.root
    }
  }

  def print(): String = {
    val builder = new mutable.StringBuilder()
    printTree("", true, builder)
    builder.toString()
  }

  def printTree(prefix: String, isTail: Boolean, builder: StringBuilder): Unit = {

    builder.append(prefix + (if (isTail) "└── " else "├── ") + toString).append(System.lineSeparator());

    for (i <- 0 until children.size - 1) {
      children(i).printTree(prefix + (if (isTail) "└── " else "├── "), false, builder);
    }

    if (children.size > 0) {
      children(children.size - 1).printTree(prefix + (if(isTail) "    " else "│   "), true, builder);
    }
  }

  override def toString = s"Node($id , h: $height, r: ${root.id})"
}