package tradeshift

class Service(persistence: Persistence, config: Config) {

  val root: Node = init()
  println(s"tree size: ${root.allChildren.size}")

  def init(): Node = {

    if(persistence.check()) {

      var id: Int = 0
      def fill(node: Node, num: Int, deep: Int): Unit = {
        if(deep > 0) {
          for(i <- 0 until num) {
            id += 1
            val temp = node.addNewChildren(s"${id}-$deep-$i")
            fill(temp, num, deep - 1)
          }
        }
      }

       val rootNode = new Node("rootNode")
       fill(rootNode, config.num, config.deep)
       persistence.save(rootNode)
       rootNode

    } else {
       persistence.load()
    }
  }

  def printChildren(nodeId: String): Either[String, String] = {
    {
      for {
        node <-root.findById(nodeId)
      } yield node.print()
    }.toRight(s"cannot find node: $nodeId")

  }

  def moveTo(nodeId: String, newParentId: String): Either[String, Unit] = {

    val res = for {
      parent <- root.findById(newParentId)
      node <- root.findById(nodeId)
    } yield parent.addAsChildren(node)

    res.map(_ => persistence.addNodeMoveLog(nodeId, newParentId))
    res.toRight(s"cannot move node: $nodeId to node: $newParentId")
  }
}

case class Config(num: Int, deep: Int)