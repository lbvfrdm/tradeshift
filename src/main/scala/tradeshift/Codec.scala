package tradeshift

import scala.annotation.tailrec
import scala.collection.mutable

class Codec(marker: String = "#", delimiter: String = "<>") {

  def serialize(root: Node): String = {

    @tailrec
    def rec(current: Node = root, siblings: Seq[Node] = Seq.empty, builder: StringBuilder = new mutable.StringBuilder()): String = {

      builder.append(current.id)
             .append(delimiter);

      val children = current.children

      if(children.nonEmpty) {

        val head +: tail = children
        rec(head, tail, builder);

      } else {

        if(siblings.nonEmpty) {

          val head +: tail = siblings
          rec(head, tail, builder.append(marker).append(delimiter))

        } else {

          findParentRec(current, builder) match {
            case None => builder.append(marker).append(delimiter).toString()
            case Some(node) => rec(node, Seq.empty, builder.append(marker).append(delimiter))
          }
        }
      }
    }

    @tailrec
    def findParentRec(node: Node, builder: mutable.StringBuilder): Option[Node] = {

      node.parent match {

        case None => None
        case Some(p) => {

          val children = p.children
          val index = children.indexOf(node)
          if (index < children.size - 1) {
            Some(children(index + 1))
          } else {
            findParentRec(p, builder.append(marker).append(delimiter))
          }
        }
      }
    }

    rec()
  }


  def deserialize(str: String): Node = {

    val head :: tail = str.split(delimiter).toList
    val root = new Node(head)

    @tailrec
    def rec(currentNode: Node, remainder: List[String]): Unit = {

      if (remainder.nonEmpty) {
        val head :: tail = remainder

        if (head.equals(marker)) {
          if (tail.nonEmpty) {
            rec(currentNode.parent.get, tail)
          }
        } else {
          val node = currentNode.addNewChildren(head)
          rec(node, tail)
        }
      }
    }

    rec(root, tail)
    root
  }
}